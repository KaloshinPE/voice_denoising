import torch
import torch.nn as nn
from torch.utils.data import Dataset
import os
from tqdm import tqdm_notebook
from torch.utils.tensorboard import SummaryWriter
import numpy as np

from utils import interm_accuracy, masked_cross_entropy, to_tensor


def MSE_masked_loss(noisy, clear, zeros, masks=None):
    '''
    Calculates MSE for batches of sequences with variable lengths padded to
        maximum value in batch

    noisy: batch with noisy data, (batch_size, 1, X, Y)
    clean: batch with clean data, (batch_size, 1, X, Y)
    zeros: number of zeros added to each sequence with padding, (batch_size)
    masks: boolean mask for some rows that we don't want to consider,
                (batch_size, 1, X)

    return: mse for batch
    '''
    noisy = noisy.squeeze(1)
    clear = clear.squeeze(1)

    if masks is not None:
        noisy = noisy * masks
        clear = clear * masks

    n_rows = noisy.shape[1]
    if len(noisy.shape) == 3:
        n_freqs = noisy.shape[2]
    else:
        n_freqs = 1

    if zeros is not None:
        denominators = (n_rows - zeros).unsqueeze(1) + 1e-5
    else:
        denominators = n_rows

    denominators *= n_freqs

    if masks is not None:
        denominators -= torch.sum(masks, 1, keepdim=True)

    square = ((noisy - clear)**2).reshape(len(noisy), -1)
    rez = torch.sum(square, 1)/denominators

    return torch.mean(rez)


def background_loss(pred_logits, pred_values, masks, values, zeros):
    """
    Calculates loss for background prediction, which consists of cross_entropy
        for background row prediction and mse on true background rows
    pred_logits: logits for rows
    pred_values: predicted background values for rows
    masks: real masks for background rows
    values: real values for background rows
    zeros: number of zeros added to each sequence with padding, (batch_size)

    return: cross_entropy, mse, mask for calculated loss
    """
    batch_size, n_rows = pred_logits.shape[:2]
    if zeros is not None:
        lens = n_rows - zeros
        cross_entropy, loss_mask = masked_cross_entropy(pred_logits, masks, lens)
    else:
        cross_entropy = nn.CrossEntropyLoss(pred_logits, masks)
        loss_mask = torch.ones_like(pred_values)

    mse = MSE_masked_loss(pred_values, values, zeros, masks)

    return cross_entropy, mse, loss_mask


def collate_fn(batch):
    """
    return: (batch_size, 1, X, Y) - clean batch,
            (batch_size, 1, X, Y) - noisy batch
            (batch_size) - indexes of samples in dataset
            (batch_size) - number of zeros added to each sequence with padding
    """
    out_clean = []
    out_noisy = []

    inds = []
    zeros = []
    max_len = 0
    for idx, clean, noisy in batch:
        inds.append(idx)
        assert len(clean) == len(noisy)
        nmax = len(clean)
        if nmax > max_len:
            max_len = nmax

    for _, clean, noisy in batch:
        n_zeros = max_len - len(clean) + 1
        zeros.append(n_zeros)
        out_clean.append(to_tensor(np.pad(clean, ((0, n_zeros), (0, 0)))))
        out_noisy.append(to_tensor(np.pad(noisy, ((0, n_zeros), (0, 0)))))

    out_clean = torch.cat(out_clean, 0)
    out_noisy = torch.cat(out_noisy, 0)
    zeros = torch.LongTensor(zeros)

    return out_clean, out_noisy, inds, zeros


def collate_fn_bg(batch):
    """
    collate function for background prediction
    return: (batch_size, 1, X, Y) - clean batch,
            (batch_size, 1, X, Y) - noisy batch
            (batch_size) - indexes of samples in dataset
            (batch_size) - number of zeros added to each sequence with padding
            (batch_size, 1, X) - masks for background rows
            (batch_size, 1, X) - mean values for background rows in clean
            samples
    """
    out_clean, out_noisy, inds, zeros = collate_fn(batch)
    out_clean = out_clean.squeeze()
    masks = []
    values = []
    for elem, z in zip(out_clean, zeros):
        elem = elem.detach().numpy()
        mask = torch.LongTensor(np.var(elem, axis=1) < 0.08)
        mask[-z:] = 0
        means = torch.FloatTensor(np.mean(elem, axis=1))
        masks.append(mask.unsqueeze(0))
        values.append(means.unsqueeze(0))
    masks = torch.cat(masks, 0)
    values = torch.cat(values, 0)
    return out_clean, out_noisy, inds, zeros, masks, values


class SpectrogramDataset(Dataset):
    def __init__(self, clean, noisy, norm_fun=None):
        self.clean = clean
        self.noisy = noisy
        self.mapping = []
        for key, val in self.clean.items():
            for i in range(len(val)):
                self.mapping.append((key, i))
        self.norm_fun = norm_fun

    def __len__(self):
        return len(self.mapping)

    def __getitem__(self, idx):
        key, i = self.mapping[idx]
        clean = self.clean[key][i]
        noisy = self.noisy[key][i]
        if self.norm_fun is not None:
            clean = self.norm_fun(clean)
            noisy = self.norm_fun(noisy)
        return idx, clean, noisy


def validate(model, test_loader, criterion, jupyter=True,
             mode='classificaiton',
             reg_scale=1., bg_alpha=1.):
    """
    jupyter: True if we run this function in jupyter
    mode: classification|regression|bg_regression
    reg_scale: if samples are rescaled, loss value should be multiplied by
            reg_scale**2 to get real mse
    bg_alpha: weight for cross_entropy in background prediction
    """
    if jupyter:
        tqdm = tqdm_notebook
    else:
        tqdm = lambda x: x

    false_clean = []
    false_noisy = []
    total_loss = 0
    total_acc = 0
    total_mse = 0
    total_omse = 0
    total_nll = 0
    with torch.no_grad():
        for i, elem in enumerate(tqdm(test_loader)):
            if mode == 'bg_regression':
                clean, noisy, inds, zeros, masks, values = elem
                masks = masks.to(model.device)
                values = values.to(model.device)
            else:
                clean, noisy, inds, zeros = elem
            clean = clean.to(model.device)
            noisy = noisy.to(model.device)
            zeros = zeros.to(model.device)

            if mode == 'classification':
                clean_out = model(clean, zeros)
                noisy_out = model(noisy, zeros)

                acc, fc, fn = interm_accuracy(clean_out, noisy_out, inds)
                total_acc += acc
                false_clean += fc
                false_noisy += fn

                labels = torch.LongTensor([0]*len(clean)).to(model.device)
                total_loss += (criterion(clean_out, 1 + labels)
                        + criterion(noisy_out, labels))/2

            elif mode == 'regression':
                denoised = model(noisy)
                loss = criterion(denoised, clean, zeros)
                total_loss += loss
                total_mse += loss * reg_scale**2

            elif mode == 'bg_regression':
                n = model.n
                pred_logits, pred_values = model(noisy)
                masks = masks[:, n:-n]
                values = values[:, n:-n]

                pred_logits, pred_values = model(noisy)
                nll, mse, loss_mask = criterion(pred_logits, pred_values, masks, values, zeros)
                total_loss += bg_alpha * nll + mse

                pred_masks = torch.argmax(pred_logits, axis=-1)
                acc = torch.sum((pred_masks == masks) *
                                loss_mask).float()/(noisy.shape[0]*noisy.shape[2] - torch.sum(zeros))

                denoised = model.transform_batch(noisy, pred_masks.bool(),
                                                 pred_values).squeeze()
                omse = MSE_masked_loss(denoised, clean[:, n:-n], zeros)

                assert acc <= 1
                total_acc += acc
                total_mse += mse
                total_nll += nll
                total_omse += omse

    N = len(test_loader)
    if mode == 'classification':
        return total_acc/N, total_loss/N, false_clean, false_noisy
    elif mode == 'regression':
        return total_mse/N, total_loss/N
    elif mode == 'bg_regression':
        return total_omse/N, total_acc/N, total_nll/N, total_mse/N, total_loss/N


def train(model, optimizer, criterion, train_loader, test_loader, n_epochs=10,
          n_verbose=20, jupyter=True, checkpoints=None, mode='classification',
          reg_scale=1., bg_alpha=1.):
    """
    n_verbose: period of log output in batches
    jupyter: True if we run this function in jupyter
    checkpoints: path to checkpoints folder
    mode: classification|regression|bg_regression
    reg_scale: if samples are rescaled, loss value should be multiplied by
            reg_scale**2 to get real mse
    bg_alpha: weight for cross_entropy in background prediction
    """
    writer = SummaryWriter(f'runs/{mode}')

    if jupyter:
        tqdm = tqdm_notebook
    else:
        tqdm = lambda x: x

    torch.autograd.set_detect_anomaly(True)
    for epoch in range(n_epochs):
        running_loss = 0
        running_acc = 0
        running_mse = 0
        running_omse = 0
        running_nll = 0

        total_loss = 0
        total_acc = 0
        total_mse = 0
        total_omse = 0
        total_nll = 0

        for i, elem in enumerate(tqdm(train_loader)):
            if mode == 'bg_regression':
                clean, noisy, inds, zeros, masks, values = elem
                masks = masks.to(model.device)
                values = values.to(model.device)
            else:
                clean, noisy, inds, zeros = elem
            clean = clean.to(model.device)
            noisy = noisy.to(model.device)
            zeros = zeros.to(model.device)

            optimizer.zero_grad()

            if mode == 'classification':
                clean_out = model(clean, zeros)
                noisy_out = model(noisy, zeros)

                labels = torch.LongTensor([0]*len(clean)).to(model.device)
                loss = (criterion(clean_out, 1 + labels)
                        + criterion(noisy_out, labels))/2

                writer.add_scalar('clean_out',
                            clean_out[0, 0],
                            epoch * len(train_loader) + i)
                writer.add_scalar('noisy_out',
                            noisy_out[0, 0],
                            epoch * len(train_loader) + i)

                acc, fc, fn = interm_accuracy(clean_out, noisy_out, inds)
                assert acc <= 1
                total_acc += acc
                running_acc += acc

            elif mode == 'regression':
                denoised = model(noisy)
                loss = criterion(denoised, clean, zeros)
                total_mse += loss * reg_scale**2
                running_mse += loss * reg_scale**2

            elif mode == 'bg_regression':
                n = model.n
                pred_logits, pred_values = model(noisy)
                masks = masks[:, n:-n]
                values = values[:, n:-n]

                nll, mse, loss_mask = criterion(pred_logits, pred_values, masks, values, zeros)
                loss = bg_alpha * nll + mse

                pred_masks = torch.argmax(pred_logits, axis=-1)
                acc = torch.sum((pred_masks == masks) *
                                loss_mask).float()/(noisy.shape[0]*noisy.shape[2] - torch.sum(zeros))

                denoised = model.transform_batch(noisy, pred_masks.bool(),
                                                 pred_values).squeeze()
                omse = MSE_masked_loss(denoised, clean[:, n:-n], zeros)

                assert acc <= 1
                total_acc += acc
                running_acc += acc
                total_mse += mse
                running_mse += mse
                total_nll += nll
                running_nll += nll
                total_omse += omse
                running_omse += omse

            loss.backward()
            optimizer.step()

            running_loss += loss.item()
            total_loss += loss.item()
            if i % n_verbose == n_verbose - 1:
                if mode == 'classification':
                    print(f'[{epoch}, {i+1}] avg_acc: {running_acc/n_verbose*100:.2f} avg_loss: {running_loss/n_verbose:.3f}')
                elif mode == 'regression':
                    print(f'[{epoch}, {i+1}] avg_mse: {running_mse/n_verbose} avg_loss: {running_loss/n_verbose}')

                elif mode == 'bg_regression':
                    print(f'[{epoch}, {i+1}] avg_omse: {running_omse/n_verbose}, avg_acc: {running_acc/n_verbose*100:.2f} avg_nll: {running_nll/n_verbose*100:.2f} avg_mse: {running_mse/n_verbose} avg_loss: {running_loss/n_verbose:.3f}')

                writer.add_scalar('training loss',
                            running_loss / n_verbose,
                            epoch * len(train_loader) + i)
                running_loss = 0.
                running_acc = 0.
                running_mse = 0.
                running_nll = 0.
                running_omse = 0.

        print('validating')
        ret = validate(model, test_loader, criterion, jupyter, mode)
        if mode == 'classification':

            print(f'epoch {epoch} train | acc: {total_acc/len(train_loader)*100:.2f}, loss: {total_loss/len(train_loader)}')
            test_acc, test_loss, _, _ = ret
            print(f'epoch {epoch} test | acc: {test_acc*100:.2f}, loss: {test_loss}')

        elif mode == 'regression':
            test_mse, test_loss = ret
            print(f'epoch {epoch} train | mse: {total_mse/len(train_loader)}, loss: {total_loss/len(train_loader)}')
            print(f'epoch {epoch} test | mse: {test_mse}, loss: {test_loss}')

        elif mode == 'bg_regression':
            test_omse, test_acc, test_nll, test_mse, test_loss = ret
            print(f'epoch {epoch} train | omse: {total_omse/len(train_loader)}, acc: {test_acc*100:.2f}, nll: {total_nll/len(train_loader)}, mse: {total_mse/len(train_loader)}, loss: {total_loss/len(train_loader)}')
            print(f'epoch {epoch} test |  omse: {test_omse}, acc: {test_acc*100:.2f}, nll: {test_nll}, mse: {test_mse}, loss: {test_loss}')

        if checkpoints is not None:
            os.makedirs(checkpoints, exist_ok=True)
            if mode == 'classification':
                fname = os.path.join(checkpoints,
                                     f'epoch_{epoch}__acc_{test_acc*100:.2f}.pth')
            elif mode == 'regression':
                fname = os.path.join(checkpoints,
                                     f'epoch_{epoch}__mse_{test_mse:.4f}.pth')
            elif mode == 'bg_regression':
                fname = os.path.join(checkpoints,
                                     f'epoch_{epoch}__loss_{test_loss:.4f}.pth')
            print(f'saving checkpoint to {fname}')
            torch.save(model.state_dict(), fname)
