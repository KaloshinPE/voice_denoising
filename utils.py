import numpy as np
import torch
import os
import torch.nn as nn


def to_tensor(x):
    return torch.FloatTensor(x).unsqueeze(0).unsqueeze(0)


def choose_device(device):
    if device is None:
        if torch.cuda.is_available():
            device = 'cuda'
        else:
            device = 'cpu'
    return device


def _sequence_mask(sequence_length, max_len=None):
    if max_len is None:
        max_len = sequence_length.data.max()
    batch_size = sequence_length.size(0)
    seq_range = torch.range(0, max_len - 1).long()
    seq_range_expand = seq_range.unsqueeze(0).expand(batch_size, max_len)
    seq_range_expand = torch.autograd.Variable(seq_range_expand)
    if sequence_length.is_cuda:
        seq_range_expand = seq_range_expand.cuda()
    seq_length_expand = (sequence_length.unsqueeze(1)
                         .expand_as(seq_range_expand))
    return seq_range_expand < seq_length_expand


def masked_cross_entropy(logits, target, length):
    """
    logits: A Variable containing a FloatTensor of size
        (batch, max_len, num_classes) which contains the
        unnormalized probability for each class.
    target: A Variable containing a LongTensor of size
        (batch, max_len) which contains the index of the true
        class for each corresponding step.
    length: A Variable containing a LongTensor of size (batch,)
        which contains the length of each data in a batch.

    return: an average loss value masked by the length, lenth mask
    """

    # logits_flat: (batch * max_len, num_classes)
    logits_flat = logits.view(-1, logits.size(-1))
    # log_probs_flat: (batch * max_len, num_classes)
    log_probs_flat = nn.functional.log_softmax(logits_flat)
    # target_flat: (batch * max_len, 1)
    target_flat = target.reshape(-1, 1)
    # losses_flat: (batch * max_len, 1)
    losses_flat = -torch.gather(log_probs_flat, dim=1, index=target_flat)
    # losses: (batch, max_len)
    losses = losses_flat.view(*target.size())
    # mask: (batch, max_len)
    mask = _sequence_mask(sequence_length=length, max_len=target.size(1))
    losses = losses * mask.float()
    loss = losses.sum() / length.float().sum()
    return loss, mask


def interm_accuracy(clean_out, noisy_out, inds):
    """
    Calculates the batch classification accuracy
    clean_out: network output on clear samples
    noisy_out: network output on noisy samples
    inds: indexes of samples

    return: accuraty, indexes of false clean elements, indexes of false noisy
elements
    """
    clean_false_mask = np.argmax(clean_out.cpu().detach().numpy(),
                                 axis=1) == 0
    false_clean = list(np.array(inds)[clean_false_mask])
    noisy_false_mask = np.argmax(noisy_out.cpu().detach().numpy(),
                                 axis=1) == 1
    false_noisy = list(np.array(inds)[noisy_false_mask])

    return 1 - (np.sum(clean_false_mask) + np.sum(noisy_false_mask))/2/len(clean_out),\
            false_clean, false_noisy


def load_best_model(model, checkpoints, best_max=True):
    """
    Inplace loading of weights from checkpoints folder according to metrix in
    their names
    model: one of the defined models
    checkpoints: folder with checkpoints with names in format
        {something}__{metric}.pth. Metric is the key for choosing best one
    best_max: True if bigger metric is better, False otherwise

    return: model with weights loaded (loading is in-place anyway)
    """
    elems = os.listdir(checkpoints)
    if best_max:
        argchoice = np.argmax
    else:
        argchoice = np.argmin
    best = argchoice([float(elem[:-4].split('_')[-1]) for elem in elems])
    print(f'loading {elems[best]}')
    model.load_state_dict(torch.load(os.path.join(checkpoints, elems[best])))
    return model


def train_test_split(clean, noisy, r=.8):
    """
    Splits dict in data into train and test groups
    clean: dict of the format {'number of speacer': [sample1, sample2, ...],
            ...} with clean samples
    noisy: sampe format as for clean, but noisy samples
    r: ratio of train part

    return: train and test dicts with the same format
    """
    keys = list(clean.keys())
    r = int(len(keys)*r)

    train_keys = keys[:r]
    test_keys = keys[r:]

    train_clean = {k: clean[k] for k in train_keys}
    train_noisy = {k: noisy[k] for k in train_keys}
    test_clean = {k: clean[k] for k in test_keys}
    test_noisy = {k: noisy[k] for k in test_keys}

    return train_clean, train_noisy, test_clean, test_noisy


def read_dataset(datapath):
    """
    Reads dataset of the proposed format
    datapath: path to top level directory

    return: train and val dicts in format {'number of speacer':
        [sample1, sample2, ...], ...}, samples are numpy arrays
    """
    train_numbers = np.array(os.listdir(os.path.join(datapath, 'train/clean'))).astype(int)
    val_numbers = np.array(os.listdir(os.path.join(datapath, 'val/clean'))).astype(int)

    def read_dir(dir_):
        ret = []
        for elem in sorted(os.listdir(dir_)):
            elem = os.path.join(dir_, elem)
            ret.append(np.load(elem))
        return ret

    train_clean = {number: read_dir(os.path.join(datapath, 'train/clean', str(number))) for number in train_numbers}
    train_noisy = {number: read_dir(os.path.join(datapath, 'train/noisy', str(number))) for number in train_numbers}
    val_clean = {number: read_dir(os.path.join(datapath, 'val/clean', str(number))) for number in val_numbers}
    val_noisy = {number: read_dir(os.path.join(datapath, 'val/noisy', str(number))) for number in val_numbers}
    return train_clean, train_noisy, val_clean, val_noisy
