# our base image
# FROM python:3-onbuild
FROM nvcr.io/nvidia/pytorch:19.07-py3 
WORKDIR /code
COPY . . 

# specify the port number the container should expose
RUN apt-get update \
    && apt-get install -y \
        vim
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
EXPOSE 8888 
