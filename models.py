import torch
import torch.nn as nn
import numpy as np
from utils import choose_device, to_tensor


class Classifier(nn.Module):
    def __init__(self, device=None, gradient_clip=None, blocks=(24, 48, 48)):
        """
        device: 'cpu'|'cuda', if None, then cuda is chosen if available
        gradient_clip: value to clip gradients with
        blocks: channel numbers for convolutional layers
        """
        super().__init__()
        self.conv_layers = []
        ch_in = 1
        out_f = 80
        self.blocks = blocks
        for ch_out in blocks[:-1]:
            self.conv_layers.append(nn.Conv2d(ch_in, ch_out, (3, 3),
                                              padding_mode=None))
            out_f -= 2
            self.conv_layers.append(nn.MaxPool2d((2, 2)))
            out_f = out_f // 2
            ch_in = ch_out
        self.conv_layers.append(nn.Conv2d(ch_in, blocks[-1], (3, 3),
                                          padding_mode=None))
        self.conv_layers = nn.Sequential(*self.conv_layers)
        out_f -= 2

        self.fc = nn.Linear(out_f * blocks[-1], 2)
        if gradient_clip is not None:
            for p in self.parameters():
                p.register_hook(lambda grad: torch.clamp(grad, -gradient_clip,
                                                         gradient_clip))
        self.device = choose_device(device)
        self.to(self.device)

    def to_final_shape(self, x):
        for block in self.blocks:
            x = (x - 1) // 2
        x -= 1
        return x

    def forward(self, x, zeros=None):
        if zeros is not None:
            zeros = self.to_final_shape(zeros).to(self.device)
        x = self.conv_layers(x)
        x = torch.transpose(x, 2, 1)
        batch_size, n_rows = x.shape[:2]
        x = x.reshape(batch_size * n_rows, -1)
        x = self.fc(x).reshape(batch_size, n_rows, 2)
        if zeros is None:
            denominators = n_rows
        else:
            denominators = (n_rows - zeros).unsqueeze(1) + 1e-5
        x = torch.sum(x, 1)/denominators

        return x

    def process_image(self, img):
        x = torch.FloatTensor(img).unsqueeze(0).unsqueeze(0).to(self.device)
        x = self.forward(x)
        return bool(np.argmax(x.cpu().squeeze().detach().numpy()))


class Regressor(nn.Module):
    def __init__(self, device=None, gradient_clip=None, blocks=((1, 16), (16, 8))):
        """
        device: 'cpu'|'cuda', if None, then cuda is chosen if available
        gradient_clip: value to clip gradients with
        blocks: channel numbers for convolutional layers
        """
        super().__init__()

        def block(ch_in, ch_out):
            return nn.ModuleList([nn.Conv2d(ch_in, ch_out, (3, 3)),
                                  nn.ReLU(inplace=True),
                                  nn.MaxPool2d((2, 2), return_indices=True)])

        def reversed_block(ch_in, ch_out):
            return nn.ModuleList([nn.MaxUnpool2d((2, 2)),
                                 nn.ConvTranspose2d(ch_out, ch_in, (3, 3)),
                                  ])

        self.encoder = nn.ModuleList()
        self.decoder = nn.ModuleList()

        for ch_in, ch_out in blocks:
            self.encoder.append(block(ch_in, ch_out))
            self.decoder.append(reversed_block(ch_in, ch_out))
        self.decoder = self.decoder[::-1]

        if gradient_clip is not None:
            for p in self.parameters():
                p.register_hook(lambda grad: torch.clamp(grad, -gradient_clip,
                                                         gradient_clip))
        self.device = choose_device(device)
        self.to(self.device)

    def encode(self, x):
        ret_shapes, ret_inds = [], []

        for conv1, relu1, pool in self.encoder:
            x = relu1(conv1(x))
            ret_shapes.append(x.shape)
            x, inds = pool(x)
            ret_inds.append(inds)

        return x, ret_shapes, ret_inds

    def decode(self, x, ret_shapes, ret_inds):
        for unpool, tconv1 in self.decoder:
            x = unpool(x, ret_inds.pop(), output_size=ret_shapes.pop())
            x = tconv1(x)
        return x

    def forward(self, x):
        x = x + self.decode(*self.encode(x))  # identical transform in worst case
        return x

    def process_image(self, img):
        x = to_tensor(img).to(self.device)
        x = self.forward(x)
        return x.cpu().squeeze().detach().numpy()


class BackgroundRegressor(nn.Module):
    def __init__(self, device=None, gradient_clip=None):
        """
        device: 'cpu'|'cuda', if None, then cuda is chosen if available
        gradient_clip: value to clip gradients with
        """
        super().__init__()

        self.convs = nn.Sequential(nn.Conv2d(1, 3, (3, 3), padding=0),
                                   nn.Conv2d(3, 9, (3, 3), padding=0)
                                   )
        self.n = len(self.convs)

        L = (80 - 2*self.n)
        self.fc = nn.Linear(L*9, L, bias=True)
        self.fc_classification = nn.Linear(L, 2, bias=True)
        self.fc_regression = nn.Linear(L, 1, bias=True)

        if gradient_clip is not None:
            for p in self.parameters():
                p.register_hook(lambda grad: torch.clamp(grad, -gradient_clip,
                                                         gradient_clip))
        self.device = choose_device(device)
        self.to(self.device)

    def forward(self, x):
        x = self.convs(x)
        x = torch.transpose(x, 2, 1)
        batch_size, n_rows = x.shape[0:2]
        x = x.reshape(batch_size*n_rows, -1)
        x = self.fc(x)

        logits = self.fc_classification(x).reshape(batch_size, n_rows, 2)
        values = self.fc_regression(x).reshape(batch_size, n_rows)
        return logits, values

    def get_mask_vals(self, img):
        x = to_tensor(img).to(self.device)
        logits, values = self.forward(x)
        probs = np.argmax(x.cpu().squeeze().detach().numpy(),
                          axis=1).reshape(-1)
        mask = np.pad(probs, (1, 1)).astype(bool)
        values = np.pad(values.cpu().squeeze().detach().numpy(), (1, 1))
        return mask, values

    def apply_mask_vals(self, img, mask=None, values=None):
        if mask is None or values is None:
            mask, values = self.get_mask_vals(img)
        img = img.copy()
        img[mask] = np.expand_dims(values[mask], 1) * np.ones((1, 80))
        return img

    def transform_batch(self, batch, masks, values):
        n = self.n
        ret = batch.squeeze(1)[:, n:-n].clone()
        ret[masks] = values[masks].unsqueeze(1) * torch.ones((1,
                                                                80)).to(self.device)
        return ret.unsqueeze(1)
