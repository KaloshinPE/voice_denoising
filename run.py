import argparse
import os
import torch
import numpy as np
import matplotlib.pyplot as plt

from models import Classifier, Regressor


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--mode', type=str, default='classification',
                        help='classification or regression')
    parser.add_argument('--data', type=str, metavar='PATH', default=None,
                        help='path to folder with spectragramms to process')
    parser.add_argument('--results', type=str, metavar='PATH',
                        default='results',
                        help='path to store results for data processing')
    parser.add_argument('--sample', type=str, metavar='PATH', default=None,
                        help='path to specific data sample. It will be\
                        visualized')
    parser.add_argument('--checkpoint', required=True, type=str, metavar='PATH',
                        help='checkpoint to load')
    return parser.parse_args()


def main():
    args = parse_args()
    if args.mode == 'classification':
        model = Classifier(gradient_clip=50, blocks=(24, 24, 48, 48))
    elif args.mode == 'regression':
        model = Regressor(gradient_clip=50, blocks=((1, 16), (16, 32), (32, 64), (64, 128)))

    model.load_state_dict(torch.load(args.checkpoint))

    if args.data is not None:
        os.makedirs(args.results, exist_ok=True)
        logfile = os.path.join(args.results, 'log.tsv')

        with open(logfile, 'w') as f:
            f.write(f'id\torigin\tclean\n')

        id_ = len(os.listdir(args.results))

        for elem in os.listdir(args.data):
            if elem.endswith('.npy'):
                impath = os.path.join(args.data, elem)
                img = np.load(impath)

                rez = model.process_image(img)
                if args.mode == 'classification':
                    with open(logfile, 'a') as f:
                        f.write(f'{id_}\t{impath}\t{rez}\n')
                elif args.mode == 'regression':
                    with open(logfile, 'a') as f:
                        f.write(f'{id_}\t{impath}\t{None}\n')
                        np.save(os.path.join(args.results, f'{id_}.npy'), rez)
                id_ += 1

    if args.sample:
        img = np.load(args.sample).astype(float)
        rez = model.process_image(img)

        if args.mode == 'classification':
            plt.imshow(img.T)
            rez = 'clean' if rez else 'noisy'
            plt.title(f'classified as {rez}', fontsize=15)
            print(f'classified as {rez}')
            plt.show()

        elif args.mode == 'regression':
            _, (ax1, ax2) = plt.subplots(2, 1)
            ax1.imshow(img.T)
            ax1.set_title('original', fontsize=15)

            ax2.imshow(rez.T)
            ax2.set_title('denoised', fontsize=15)

            plt.show()
            print(f'mse: {np.mean((img - rez)**2)}')


if __name__ == '__main__':
    main()
