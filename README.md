# Задание 1
Лежит в ноутбуке max_subarray.ipynb, функция в первой ячейке + несколько ее вызовов на граничных примерах в последующих. Работает за один проход, константная дополнительная память.

# Задание 2
## Как запустить?

* **Докер** <br>
`docker build -t voice_denoising .`
`docker run --rm -it --gpus all --shm-size 4G -v ${path_to_dataset}:/data -v ${desired_results_folder}:/results voice_denoising /bin/bash` 

**или** загрузить с докер хаба `sonaru/voice_denoising`


* **Классификация** <br>
`python run.py --sample /data/train/noisy/20/20_205_20-205-0004.npy --mode classification --checkpoint checkpoints/best_checkpoints/classification_best.pth` - запустить классификацию отдельного сэмпла. Выводит ее на экран (в докере, соответственно, не будет работать) и пишет результат в консоль.<br><br>
`python run.py --mode classification --data test_samples --results test_results --checkpoint checkpoints/best_checkpoints/classification_best.pth` - проведет классификацию для всех test_samples/*.npy файлов, результаты положит в test_results/log.tsv


* **Удаление шума** (аналогичные команды) <br>
`python run.py --sample /data/train/noisy/20/20_205_20-205-0004.npy --mode regression --checkpoint checkpoints/best_checkpoints/regression_best.pth`<br>
`python run.py --mode regression --data test_samples --results test_results --checkpoint checkpoints/best_checkpoints/regression_best.pth` <br>


* **Обучение**
`python train.py --mode regression --data /data/ --checkpoints /results/checkpoints --learning-rate 0.01 --epochs 30` - mode  classification или regression

## Что было сделано?

В обоих заданиях я сначала попытался придумать некие простые методы решения, чтобы потом было с чем сравнивать более сложные.
(Почти) все изыскания описаны в ноутбуке explore_data.ipynb

 ### Классификация
 
 * **Бэйзлайн** <br>
    Шум добавляет некоторый фон на данные, то есть повышает среднее по спектрограмме значение сигнала.
    Поэтому мне показалось перспективным взять как признак значение 5% квантили по каждой частоте и попробовать обучить на этом логистическую регрессию.
    Более сложные модели на этом признаке использовать не очень осмысленно, так как мы по-сути подбираем лишь порог отсечения.
    Тестировался я тут кросс-валидацией, получил качество более 90% на трейне а затем 70% на валидации, что означает, что данные из разных доменов.
    Отнормировав валидационные данные, мне удалось получить на них accuracy 91.4% Примечательно,
    что нейросети для обеих задач на нормированных подобным образом данных показывали себя хуже, чем на чистых.

 * **Сетка** <br>
    Классификация производилась небольшой сетью (из нескольких сверточных слоев и одного полносвязного). Мы берем картинку (X, C_in, 1), сворачиваем
    в (Y, C_out, K), далее для каждого элемента  (i, :, :) предсказываем вероятность того, что он шумный и усредняем для всех имеющихся.
    После небольшого подбора архитектур получаем качество 96.4% на валидации.

 * **Как улучшать результат?** <br>
   Была выбрана маленькая сеть, потому что тренировать ее можно на ноутбуке и выдает она приемлемое качество. Тем не менее, переобучить сетку
    такого размера на трэйне мне не удалось, а значит, что есть простор для применения сеток побольше (resnet18, mobilenet, ...). Помимо этого,
    можно поисследовать влияние области видимости сверточной части, прикрутив, например, atrous свертки.

### Удаление шума
* **Данные** <br>
    Исходно mse по валидационной выборке 0.1328. При этом данные достаточно разнородные: где-то чистые записи выглядят как зашумленные,
    где-то шум имеет свою периодическую структуру, где-то на чистых данных он удален алгоритмически (просто куски записи обнулены).

* **Бэйзлайн** <br>
    Было испробовано несколько подходов, все они заключались в эвристическом определении шумных пикселей и подстановке вместо них некоего нулевого значения.
    Сам по себе выбор этого значения по шумной спектрограмме требует выбора эвристики. Шумные пиксели я пытался выделять тремя способами: отсекая по квантили амплитуды,
    выделяя моменты времени с низкой дисперсией амплитуд по частотам (равномерное распределение скорее всего означает, что ничего, кроме шума, в этот момент не происходит),
    выделяя моменты с периодическими пиками на частотной характеристике (голос это набор гармоник с расстоянием в октаву). Все эти методы работали на отдельных примерах,
    но без подбора параметров давали mse больше 0.13. Последний метод, по моему мнению, все же может быть доработан до хорошего уровня при правильной нормировке
    данных и при учете того, что высота голоса в течение фразы меняется плавно, но в любом случае, кажется, алгоритмические подходы - не то, что было интересно в рамках данной задачи.

* **Сетка для регрессии** <br>
    Первый (и сработавший) нейросетевой подход - предсказывать, как нужно изменить исходное изображение, чтобы убрать шум. Пусть X - исходная спектрограмма, X' - шумная,
    тогда мы предсказываем dX, минимизируя MSE(X' + dX - X). Это облегчает обучение сети, позволяя ей в худшем случае выучивать тождественное преобразоывание.
    В качестве самой сети был использован автоэнкодер из прямых/транспонированных сверток и пулинга/анпулинга с what-where механизмом. Лучший результат на валидации: 0.0721

* **Сетка для предсказания бэкграунда** <br>
    Второй (не сработавший, но кажущийся мне перспективным) подход заключался в том, чтобы предсказывать, в какие моменты времени ничего не происходит (и их, соответственно, можно занулить).
    Сетка для каждого момента времени сворачивала его с несколькими соседними и предсказывала два значения: вероятность того, что это фон, и значение, которым его нужно заполнить.
    Соответственно, функция потерь состояла из двух членов: кросс-энтропия для первого числа и mse для второго. Причем mse можно считать двумя способами: только для фоновых моментов времени, или для
    всех разом. Я попробовал оба. <br>
    Сгенерированная по чистым примерам разметка выдавала 0.057 MSE. Мне удалось обучить классификационную часть сетки до 80% accuracy и регрессионную до 0.07 mse. 
    Тем не менее, общий результат был хуже 0.17. Думаю, основная проблема в низкой accuracy, если мы пытаемся занулять даже изредка сильно ненулевые значения, они дают большой вклад в mse.

 * **Как улучшать результат?** <br>
    Применимо все, что сказано для классификации, но эта задача гораздо более вариативна. Можно комбинировать алгоритмы шумоподавления с сетью, можно использовать предобученную сеть для распознавания голоса
    и детектировать с ее помощью значимые куски записи/оптимизироваться на точность ее предсказаний, можно генерировать картинки в стиле GAN (добавить adversarial loss с классификатора на шум/не шум), можно много чего изобрести с аугментацией (для чего нужно подробнее
    смотреть на встречающиеся типы шума), можно прикручивать рекурентные генеративные модели для предсказания следующего нешумного момента времени по предыдущим шумным.
    