import argparse
import torch.nn as nn
from torch.utils.data import DataLoader
import torch.optim as optim

from models import SpectrogramDataset, Classifier, Regressor, collate_fn,\
                                                            MSE_masked_loss
from training import validate, train
from utils import load_best_model, train_test_split, read_dataset


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--mode', type=str, default='classification',
                        help='classification | regression | bg_regression')
    parser.add_argument('--data', type=str, metavar='PATH', default='/data',
                        help='path to dataset')
    parser.add_argument('--checkpoints', required=True, type=str, metavar='PATH',
                        help='path to store checkpoints')
    parser.add_argument('--epochs', type=int, default=30,
                        help='number of epochs')
    parser.add_argument('--verbose', type=int, default=30,
                        help='log messages interval')
    parser.add_argument('--learning-rate', type=float, default=1e-2)
    parser.add_argument('--alpha', type=float, default=1., help='weight for\
                        cross entropy, only for bg_regression')
    return parser.parse_args()


def main():
    args = parse_args()
    print('loading dataset')
    train_clean, train_noisy, val_clean, val_noisy = read_dataset(args.data)
    train_part_clean, train_part_noisy, test_part_clean, test_part_noisy = train_test_split(train_clean, train_noisy)

    if args.mode == 'classification':
        model = Classifier(gradient_clip=50)
        criterion = nn.CrossEntropyLoss()
    elif args.mode == 'regression':
        model = Regressor(gradient_clip=50)
        criterion = MSE_masked_loss

    train_dataset = SpectrogramDataset(train_part_clean, train_part_noisy)
    test_dataset = SpectrogramDataset(test_part_clean, test_part_noisy)
    print(f'train: {len(train_dataset)}, test: {len(test_dataset)}')

    train_loader = DataLoader(train_dataset, batch_size=32, shuffle=True,
                              num_workers=4, collate_fn=collate_fn)
    test_loader = DataLoader(test_dataset, batch_size=32, shuffle=True,
                             num_workers=4, collate_fn=collate_fn)

    optimizer = optim.Adam(model.parameters(), lr=args.learning_rate, weight_decay=1e-3)

    train(model, optimizer, criterion, train_loader, test_loader,
          n_epochs=args.epochs,
          n_verbose=args.verbose, jupyter=False, checkpoints=args.checkpoints,
          mode=args.mode)

    val_dataset = SpectrogramDataset(val_clean, val_noisy)
    val_loader = DataLoader(val_dataset, batch_size=32, shuffle=True,
                            num_workers=4, collate_fn=collate_fn)
    print(f'val: {len(val_dataset)}')

    if args.mode == 'classification':
        max_best = True
    elif args.mode == 'regression':
        max_best = False

    best_model = load_best_model(model, args.checkpoints, max_best)
    print('validation')
    ret = validate(best_model, val_loader, criterion, jupyter=False,
                   mode=args.mode)

    if args.mode == 'classification':
        acc, loss, _, _ = ret
        print(f'val acc: {acc*100:.2f}, loss: {loss}')
    elif args.mode == 'regression':
        mse = ret
        print(f'mse: {mse}')


if __name__ == '__main__':
    main()
